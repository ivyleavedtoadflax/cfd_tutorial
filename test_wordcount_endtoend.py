import os
import os.path
from nose.tools import assert_equal
from nose.tools import assert_true

def files_equal(file1,file2):
  cmd = "diff -q "+file1+" "+file2
  return os.system(cmd)

def test_wordcount():
  for f in os.listdir("books"):
    if f.endswith(".txt"):
      name = os.path.splitext(f)[0]
      txt = os.path.join("books",f)
      dat = name + ".dat"
      cmd = "python wordcount.py " + txt + " " + dat
      os.system(cmd)
      assert_true(os.path.isfile(dat))
      expected = os.path.join("expected", dat)
      assert_equal(0, files_equal(expected, dat))

def test_noinputfile():
  print "Test noinputfile"
  os.system("python wordcount.py books/noinput.txt noinput.dat")
  assert_true(not os.path.isfile("noinput.dat"))