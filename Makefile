# Calculate word frequencies.
# $@ is target of current rule, $< is first dependency
# $^ means all dependencies
# %.dat wildcard could be war, abyss, etc

COUNTER=python wordcount.py
PLOTTER=python plotcount.py

TEXTFILES=$(shell find books -type f -name '*.txt')
DATFILES=$(patsubst books/%.txt, %.dat, $(TEXTFILES))
JPGFILES=$(patsubst books/%.txt, %.jpg, $(TEXTFILES))

# How bloody cryptic is this damn language!

%.dat : books/%.txt
		$(COUNTER) $< $@
%.jpg : %.dat
		$(PLOTTER) $< $@

analysis.tar.gz : $(DATFILES) wordcount.py
		tar -czf $@ $^
		
.PHONY : all
all : war.dat abyss.dat bridge.dat kim.dat

.PHONY : clean
clean : 
		rm -f *.dat
		rm -f *.pyc
		rm -f *.jpg
		rm -f *.gz