#Benchmarking

##Lists

| Method | Scale Factor | Interations | Time (s)|
|--------|--------------|-------------|---------|
|Lists   |              |             |         |
|        |1             |1000         |0.9      |
|        |2             |5000         |18       |
|        |2             |10000        |35       |
|        |3             |10000        |80       |
|        |4             |10000        |144      |
|        |5             |10000        |225      |

## Creat array without list 

psi = [[0 for col in range(m+2)] for row in range(n+2)] changed to psi = np.zeros((m+2, n+2), float)

| Method | Scale Factor | Interations | Time (s)|
|--------|--------------|-------------|---------|
|Lists   |              |             |         |
|        |1             |1000         |3.8      |
|        |2             |5000         |69       |
|        |2             |10000        |_       |
|        |3             |10000        |_       |
|        |4             |10000        |_      |
|        |5             |10000        |_      |